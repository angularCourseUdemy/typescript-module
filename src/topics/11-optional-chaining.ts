export interface Passanger {
    name: string;
    children?: string[]
}

const passenger1: Passanger = {
    name: "Juan"
}

const passenger2: Passanger = {
    name: "Sebastian",
    children: ['Jose', 'Daniela']
}

const printChildren = (passenger: Passanger) => {
    const howMany = passenger.children?.length || '0';
    console.log("Cantidad: ",howMany);
}

printChildren(passenger1)