export class Prueba {

    constructor(public name: string, public phone?: string){
       this.name = name;
       this.phone = phone;
    }


}

const juan = new Prueba("Juan", "3058159117");
console.log(juan.name);

export class Hero {
    constructor(public power: string, public realName: string, public prueba: Prueba) {
    }
}

const newPerson = new Prueba('AndresNombre','Prueba2')
const ironman = new Hero("volar", "Pedro", newPerson)
console.log("Data: ",ironman);
