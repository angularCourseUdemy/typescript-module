interface AudioPlayer {
    audioVolume: number;
    songVolume: number;
    song: string;
    details: Details;
}

interface Details {
    author: string;
    year: number;
}

const audioPlayer: AudioPlayer = {
    audioVolume: 90,
    songVolume: 21,
    song: 'Prueba',
    details: {
        author: 'Juan',
        year: 2021
    }
}

const {
    audioVolume: genericVolume, 
    song: songGeneric, 
    details: {author}
} = audioPlayer;

console.log('Volume: ',genericVolume, author, songGeneric);

// const [,, p3 = 'No hay'] = ['Goku', 'Vegeta']

// console.log('Personaje: ', p3);

export {};