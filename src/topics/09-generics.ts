export function whatsMyType<T>(argument: T): T {
    return argument
}

let amIString = whatsMyType<string>('Hola');
let amIString2 = whatsMyType<number>(1234);
let amIString3 = whatsMyType<number[]>([1,2,3]);
console.log(amIString.split('a'));
console.log(amIString2.toString());
console.log(amIString3[1]);
