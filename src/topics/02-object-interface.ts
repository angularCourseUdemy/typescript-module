let skills: string[] = ['Cura','Enfrentamiento']

interface character {
    name: string;
    age: number;
    skills: string[];
    prueba: string|undefined;
}


const strider: character = {
    name: 'Juan',
    age: 23,
    skills: skills,
    prueba: ""
}

strider.prueba = 'valor de prueba'

console.table(strider)
export {}