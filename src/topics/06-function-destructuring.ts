interface Product {
    description: string;
    price: number;
}

interface TaxCaluclationOptions {
    tax: number;
    products: Product[];
}


const phone: Product = {
    description: "iPhone 14 pro",
    price: 250
}

const tablet: Product = {
    description: "iPad pro 2",
    price: 120
}

const cart = [phone, tablet];
const tax = 0.19;

export function taxCaluclation(options: TaxCaluclationOptions): [number,number]{
    let total = 0;
    options.products.forEach(({price}) => {
        total += price
    });
    return [total, total * options.tax]
}

const [ total, taxTotal ] = taxCaluclation({
    products: cart,
    tax
})
console.log("Resultado: ",total);
console.log("Tax: ",taxTotal);
export {};