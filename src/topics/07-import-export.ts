import { Products, taxCaluclation } from './06-function-destructuring'

const cart: Products[] = [
    {
        description: "iPad pro 2",
        price: 120
    },
    {
        description: "iPad pro 1",
        price: 120
    }
]

const [total, taxTotal] = taxCaluclation({
    products: cart,
    tax: 0.19
})

console.log("Resultado1: ",total);
console.log("Tax1: ",taxTotal);
